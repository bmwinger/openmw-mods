# Copyright 2019 Portmod Authors
# Distributed under the terms of the GNU General Public License v3

import os
from pybuild import Pybuild1, InstallDir, File


class Package(Pybuild1):
    NAME = "Area Effect Arrows"
    DESC = "Adds a new shop selling a wide selection of quality Marksman weapons"
    HOMEPAGE = """
        https://elderscrolls.bethesda.net/en/morrowind
        https://gitlab.com/bmwinger/umopp
    """
    # Original is all-rights-reserved
    # UMOPP is attribution
    LICENSE = "all-rights-reserved attribution"
    RESTRICT = "mirror"
    RDEPEND = "base/morrowind"
    DEPEND = ">=bin/delta-plugin-0.12"
    KEYWORDS = "openmw"
    SRC_URI = """
        https://cdn.bethsoft.com/elderscrolls/morrowind/other/area_effect_arrows.zip
        https://gitlab.com/bmwinger/umopp/uploads/ace2382c4a5dcb85076b425a8029126e/areaeffectarrows-umopp-3.1.0.tar.xz
    """
    INSTALL_DIRS = [
        InstallDir(".", PLUGINS=[File("AreaEffectArrows.esp")], S="area_effect_arrows")
    ]

    def src_prepare(self):
        # From instructions in README.md
        path = os.path.join(self.WORKDIR, "areaeffectarrows-umopp-3.1.0")
        self.execute(
            "delta_plugin -v apply " + os.path.join(path, "AreaEffectArrows.patch")
        )
