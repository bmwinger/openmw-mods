# Copyright 2019-2021 Portmod Authors
# Distributed under the terms of the GNU General Public License v3
from pybuild import File, InstallDir, Pybuild1
from pybuild.info import PV, P

BASE = "Solstheim_Tomb_of_the_Snow_Prince"
GRAPHICAL = "Solstheim_Graphical_Replacer"


class Package(Pybuild1):
    NAME = "Tomb of the Snow Prince"
    DESC = "Improves the graphical fidelity, design, and gameplay of the Bloodmoon DLC"
    HOMEPAGE = """
        https://github.com/EllisNZ/Tomb-of-The-Snow-Prince
        https://www.nexusmods.com/morrowind/mods/48525
        https://www.nexusmods.com/morrowind/mods/48422
    """
    KEYWORDS = "openmw"
    LICENSE = "all-rights-reserved"
    TEXTURE_SIZES = "1024"
    SRC_URI = f"""
        https://github.com/EllisNZ/Tomb-of-The-Snow-Prince/archive/P2-{PV}.tar.gz
        -> {P}.tar.gz
        rebirth? (
            https://gitlab.com/portmod/mirror/-/raw/master/Morrowind_Rebirth_Patch-48525-1-2-1606806569.7z
        )
    """
    # Included as a hint for Importmod
    NEXUS_SRC_URI = """
        https://www.nexusmods.com/morrowind/mods/48525?tab=files&file_id=1000022004
        -> Morrowind_Rebirth_Patch-48525-1-2-1606806569.7z
    """
    IUSE = "minimal pfp tr-preview rebirth +grass"
    RDEPEND = """
        base/morrowind[bloodmoon,tribunal]
        pfp? ( base/patch-for-purists )
        !minimal? ( landmasses/tamriel-data-7.1 )
        tr-preview? ( landmasses/tamriel-rebuilt[preview] )
        !tr-preview? ( !!landmasses/tamriel-rebuilt[preview] )
        rebirth? ( base/morrowind-rebirth )
        !rebirth? ( !!base/morrowind-rebirth )
        grass? (
            land-flora/remiros-groundcover[-solstheim]
            !!land-flora/vurts-groundcover[solstheim]
            !!land-flora/ozzy-grass[bloodmoon]
            !!land-flora/aesthesia-groundcover[bloodmoon]
        )
    """
    REQUIRED_USE = "?? ( minimal pfp )"
    S = f"{P}/Tomb-of-The-Snow-Prince-P2-{PV}/"
    INSTALL_DIRS = [
        InstallDir(f"{BASE}/000 Core", REQUIRED_USE="!minimal", S=S),
        InstallDir(
            f"{BASE}/010 Solstheim - Tomb of the Snow Prince",
            PLUGINS=[
                File("TOTSP TD Content Integration.esp"),
                File("Solstheim Tomb of The Snow Prince.esm"),
            ],
            REQUIRED_USE="!minimal",
            S=S,
        ),
        InstallDir(
            f"{BASE}/011 TOTSP Patches",
            PLUGINS=[
                # File("TOTSP_abotBoats.esp"),
                File("TOTSP_TR_Preview.esp", REQUIRED_USE="tr-preview"),
                File("TOTSP_Patch_for_Purists_4.0.2.esp", REQUIRED_USE="pfp"),
                # File("TOTSP_Forceful_Travel_NPC_Override.ESP"),
            ],
            REQUIRED_USE="!minimal",
            S=S,
        ),
        InstallDir(
            f"{BASE}/012 Armor of the Snow Prince Redux",
            PLUGINS=[File("Snow Prince Armor Redux.ESP")],
            REQUIRED_USE="!minimal",
            S=S,
        ),
        InstallDir(
            f"{BASE}/013 Missing Snow Armor Pieces",
            PLUGINS=[File("Missing snow armor.esp")],
            REQUIRED_USE="!minimal",
            S=S,
        ),
        InstallDir(f"{BASE}/014 Fierce Wolf Helms", REQUIRED_USE="!minimal", S=S),
        InstallDir(f"{BASE}/015 Hide-Like Animal Pelts", REQUIRED_USE="!minimal", S=S),
        InstallDir(
            f"{GRAPHICAL}/010 Solstheim - HD Worldspace Graphical Replacer", S=S
        ),
        InstallDir(f"{GRAPHICAL}/011 Skyrim-Like Trees", S=S),
        InstallDir(
            f"{GRAPHICAL}/012 Remiros' Groundcover for TOTSP",
            GROUNDCOVER=[File("Rem-TOTSP.esp")],
            S=S,
            REQUIRED_USE="grass",
        ),
        InstallDir(f"{GRAPHICAL}/013 Grey Solstheim Rocks", S=S),
        InstallDir(
            ".",
            PLUGINS=[File("Rebirth+TOTSP Patch.esp")],
            REQUIRED_USE="rebirth",
            S="Morrowind_Rebirth_Patch-48525-1-2-1606806569",
        ),
    ]
